import { createApp } from 'vue'
import { createStore } from 'vuex';
import App from './App.vue'

const store = createStore({
    state() {
        return {
            users: []
        }
    },
    mutations: {
        add(state, item) {
            state.users.push(item)
        }
    }
})

const app = createApp(App)
app.use(store);

app.mount('#app')
