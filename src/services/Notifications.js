import Api from './Api'

export default {
    createNotification(data) {
        return Api.post('/notifications', data)
    }
}